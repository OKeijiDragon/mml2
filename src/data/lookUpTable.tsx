// 0x12 Manda Island Ruins

const lookUpTable = {
    '0x000020': {
        name: 'Meilin',
        credit: 'https://twitter.com/johnlouissmith/status/1544020892363231232',
        stages: ['ST1900.BIN'],
    },
    '0x000120': {
        name: 'Data the Monkey',
        stages: ['ST1705.BIN', 'ST1900.BIN', 'ST3C00.BIN'],
    },
    '0x000320': {
        name: 'Jakko',
        stages: ['ST1200.BIN'],
    },
    '0x000720': {
        name: 'Giwan',
        stages: ['ST0F01.BIN'],
    },
    '0x001520': {
        name: 'Chest',
        stages: ['ST0F00.BIN', 'ST1200.BIN', 'ST1300.BIN'],
    },
    '0x001620': {
        name: 'Verner Von Bluecher',
        stages: [
            'ST0201.BIN',
            'ST0203.BIN',
            'ST0301.BIN',
            'ST0302.BIN',
            'ST0303.BIN',
            'ST1C02.BIN',
        ],
    },
    '0x001920': {
        name: 'Kuruguru',
        stages: ['ST1200.BIN', 'ST1300.BIN'],
    },
    '0x001a20': {
        name: 'Barrell Caskett',
        stages: [
            'ST0201.BIN',
            'ST0203.BIN',
            'ST0301.BIN',
            'ST0302.BIN',
            'ST0303.BIN',
            'ST1C02.BIN',
        ],
    },
    '0x001b20': {
        name: 'Inmii',
        stages: ['ST1200.BIN'],
    },
    '0x001c20': {
        name: 'Megaman in Apron',
        stages: ['ST0305.BIN'],
    },
    '0x001d20': {
        name: 'Sulphur Bottom Guard',
        stages: [
            'ST0201.BIN',
            'ST0202.BIN',
            'ST0203.BIN',
            'ST0301.BIN',
            'ST0302.BIN',
            'ST0303.BIN',
            'ST1C02.BIN',
        ],
    },
    '0x001e20': {
        name: 'Glyde Loath in Suit',
        stages: ['ST0202.BIN', 'ST0303.BIN', 'ST1C02.BIN'],
    },
    '0x001f20': {
        name: 'Roll in Pajamas',
        stages: ['ST0305.BIN'],
    },
    '0x002020': {
        name: 'Ruins Trap (rotating saw)',
        stages: ['ST1300.BIN'],
    },
    '0x002120': {
        name: 'Teisel Bonne',
        stages: ['ST0202.BIN', 'ST0303.BIN', 'ST1C02.BIN'],
    },
    '0x002220': {
        name: 'Tarble',
        stages: ['ST1200.BIN', 'ST1300.BIN'],
    },
    '0x002320': {
        name: 'Reporter',
        stages: ['ST0201.BIN', 'ST0302.BIN'],
    },
    '0x002520': {
        name: 'Camera Woman',
        stages: ['ST0201.BIN', 'ST0302.BIN'],
    },
    '0x002620': {
        name: 'Puurian',
        stages: ['ST1300.BIN'],
    },
    '0x002720': {
        name: 'Sulphur Bottom',
        stages: ['ST1C02.BIN'],
    },
    '0x002820': {
        name: 'Tron Bonne',
        stages: ['ST0202.BIN', 'ST0303.BIN', 'ST1C02.BIN'],
    },
    '0x002a20': {
        name: 'Mirumijee',
        stages: ['ST1200.BIN', 'ST1300.BIN'],
    },
    '0x002d20': {
        name: 'BirdBot Black',
        stages: ['ST1701.BIN', 'ST1800.BIN', 'ST1901.BIN'],
    },
    '0x002e20': {
        name: 'Matilda 1',
        stages: ['ST0302.BIN'],
    },
    '0x002f20': {
        name: 'Matilda 2',
        stages: ['ST0301.BIN', 'ST0302.BIN', 'ST0303.BIN'],
    },
    '0x003020': {
        name: 'Flutter',
        stages: ['ST0305.BIN', 'ST1705.BIN', 'ST3C00.BIN'],
    },
    '0x003120': {
        name: 'Green Frongel',
        stages: ['ST1200.BIN', 'ST1300.BIN'],
    },
    '0x003c20': {
        name: 'Klaymoor',
        stages: ['ST0202.BIN', 'ST0303.BIN', 'ST1C02.BIN'],
    },
    '0x003f20': {
        name: 'Gatz Combat Form',
        stages: ['ST0302.BIN', 'ST0303.BIN'],
    },
    '0x004220': {
        name: 'KTV Ship',
        stages: ['ST0305.BIN'],
    },
    '0x004e20': {
        name: 'Matilda Hand',
        stages: ['ST0301.BIN'],
    },
    '0x005420': {
        name: "Glyde's Ship",
        stages: ['ST1701.BIN'],
    },
    '0x005520': {
        name: 'Joe',
        stages: ['ST0F00.BIN'],
    },
    '0x005a20': {
        name: 'Geetz',
        stages: ['ST0F00.BIN'],
    },
    '0x005e20': {
        name: 'Parabola Gun',
        stages: ['ST1800.BIN'],
    },
    '0x006020': {
        name: 'Roll with Jacket',
        stages: ['ST0F00.BIN', 'ST1800.BIN'],
    },
    '0x006220': {
        name: 'Nino Island Cannon',
        stages: ['ST1900.BIN'],
    },
    '0x006920': {
        name: 'Glyde Loathe in Armor',
        stages: ['ST1701.BIN'],
    },
    '0x006a20': {
        name: 'Ship Wheel',
        stages: ['ST1701.BIN'],
    },
    '0x006b20': {
        name: 'Sitting Figure (Sera?)',
        stages: ['ST1C02.BIN'],
    },
    '0x006f20': {
        name: 'Blue Refractor Shard',
        stages: ['ST0F00.BIN'],
    },
    '0x010420': {
        name: 'Golbesh',
        stages: ['ST1200.BIN', 'ST1300.BIN'],
    },
    '0x010520': {
        name: 'Horokko',
        stages: ['ST0F00.BIN'],
    },
    '0x010820': {
        name: 'Fingerii',
        stages: ['ST0F00.BIN'],
    },
    '0x011520': {
        name: 'Mimic',
        stages: ['ST0F00.BIN', 'ST1300.BIN'],
    },
    '0x012d20': {
        name: 'Birdbot Red',
        stages: ['ST1701.BIN', 'ST1901.BIN'],
    },
    '0x013120': {
        name: 'Red Frongel',
        stages: ['ST1200.BIN', 'ST1300.BIN'],
    },
    '0x016020': {
        name: 'Roll Casket',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x022d20': {
        name: 'Birdbot Green',
        stages: ['ST1701.BIN', 'ST1901.BIN'],
    },
    '0x026520': {
        name: 'Megaman Hand?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x032d20': {
        name: 'Birdbot Purple',
        stages: ['ST1701.BIN', 'ST1901.BIN'],
    },
    '0x036520': {
        name: 'Megaman Special Weapon1?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x046520': {
        name: 'Megaman Special Weapon2?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x056520': {
        name: 'Megaman Special Weapon3?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x060020': {
        name: 'Parabola Gun Mechanic',
        stages: ['ST1800.BIN'],
    },
    '0x066520': {
        name: 'Megaman Special Weapon4?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x076520': {
        name: 'Megaman Special Weapon5?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x086520': {
        name: 'Megaman Special Weapon6?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x096520': {
        name: 'Megaman Special Weapon7?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x0a6520': {
        name: 'Megaman Special Weapon8?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x0b6520': {
        name: 'Megaman Special Weapon9?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x0c6520': {
        name: 'Megaman Special Weapon10?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x0d6520': {
        name: 'Megaman Special Weapon11?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x0e6520': {
        name: 'Megaman Special Weapon12?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x0f6520': {
        name: 'Megaman Special Weapon13?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x106520': {
        name: 'Megaman Special Weapon14?',
        stages: ['ST1705.BIN', 'ST1900.BIN'],
    },
    '0x1d0020': {
        name: 'Nino Island Mayor',
        stages: ['ST1705.BIN'],
    },
    '0x1e0020': {
        name: 'Nino Island Man',
        stages: ['ST1705.BIN'],
    },
    '0x270020': {
        stages: ['ST3C00.BIN'],
    },
    '0x2e0020': {
        stages: ['ST1900.BIN'],
    },
    '0x330020': {
        stages: ['ST1900.BIN'],
    },
    '0x340020': {
        stages: ['ST1900.BIN'],
    },
    '0x3c0020': {
        stages: ['ST3C00.BIN'],
    },
    '0x3d0020': {
        stages: ['ST1900.BIN'],
    },
    '0x3e0020': {
        stages: ['ST1900.BIN'],
    },
    '0x440020': {
        stages: ['ST3C00.BIN'],
    },
}

export default lookUpTable
