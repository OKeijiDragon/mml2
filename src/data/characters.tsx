import { Character } from '../types'

const characters: Character[] = [
    {
        name: 'Data the Monkey',
        id: 0x000120,
        tag: 'NPC',
        modelFile: 'ST1900.BIN',
        otherFiles: ['ST1705.BIN', 'ST3C00.BIN'],
        textures: [
            {
                imgFile: 'ST1902T.BIN',
                imgOffset: 0x05f000,
                palFile: 'ST1902T.BIN',
                palOffset: 0x05f000,
            },
        ],
        animations: ['Monkey Dance', 'Jazz Hands'],
    },
    {
        name: 'Chest',
        id: 0x001520,
        tag: 'Object',
        modelFile: 'ST1200.BIN',
        otherFiles: ['ST0F00.BIN', 'ST1300.BIN'],
        textures: [
            {
                imgFile: 'ST12T.BIN',
                imgOffset: 0x06f000,
                palFile: 'ST12T.BIN',
                palOffset: 0x06f000,
            },
        ],
        animations: ['Open Chest', 'Close Chest'],
    },
    {
        name: 'Jakko',
        id: 0x000320,
        tag: 'Enemy',
        modelFile: 'ST1200.BIN',
        otherFiles: [],
        textures: [
            {
                imgFile: 'ST12T.BIN',
                imgOffset: 0x06c800,
                palFile: 'ST12T.BIN',
                palOffset: 0x06c800,
            },
        ],
        animations: [],
    },
    {
        name: 'Golbesh',
        id: 0x010420,
        tag: 'Enemy',
        modelFile: 'ST1200.BIN',
        otherFiles: [],
        textures: [
            {
                imgFile: 'ST12T.BIN',
                imgOffset: 0x06d800,
                palFile: 'ST12T.BIN',
                palOffset: 0x06d800,
            },
        ],
        animations: [],
    },
]

export default characters
