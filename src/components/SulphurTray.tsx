import React, { useState } from 'react'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import Avatar from '@mui/material/Avatar'
import { characters } from '../data'
import { parseCharacter } from '../parse'
import { SomeMesh, Character } from '../types'
import { AnimationClip } from 'three'

export default function SulphurTray({
    width,
    setMesh,
    setAnimations,
    setAnim,
}: {
    width: number
    setMesh: (m: SomeMesh) => void
    setAnimations: (a: AnimationClip[]) => void
    setAnim: (a: string) => void
}) {
    const [active, setActive] = useState('')

    const loadCharacter = async (char: Character) => {
        const { mesh, animations } = await parseCharacter(char)
        setActive(char.name)
        setMesh(mesh)
        setAnimations(animations)
        setAnim('')
    }

    return (
        <List sx={{ width, bgcolor: 'background.paper', m: 0, p: 0 }}>
            {characters.map((char) => {
                return (
                    <ListItem
                        selected={char.name === active}
                        key={char.name}
                        divider={true}
                        onClick={() => {
                            loadCharacter(char)
                        }}
                    >
                        <ListItemAvatar>
                            <Avatar sx={{ color: '#000', fontSize: '0.7em' }}>
                                {char.tag}
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                            primary={char.name}
                            secondary={char.modelFile}
                        />
                    </ListItem>
                )
            })}
        </List>
    )
}
