import { ChangeEvent, useRef } from 'react'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import * as localForage from 'localforage'

export default function SulphurAppBar() {
    const inputRef = useRef<HTMLInputElement>(null)

    const handleImportChange = async (evt: ChangeEvent<HTMLInputElement>) => {
        const { files } = evt.target
        if (!files) {
            return
        }
        for (let i = 0; i < files.length; i++) {
            const { name } = files[i]
            const arrayBuffer = await files[i].arrayBuffer()
            await localForage.setItem(name, arrayBuffer)
        }
    }

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    Megaman Legends 2
                </Typography>
                <Button
                    color="inherit"
                    onClick={() => {
                        inputRef.current?.click()
                    }}
                >
                    Import
                </Button>
                <input
                    ref={inputRef}
                    type="file"
                    style={{ display: 'none' }}
                    accept=".bin, .BIN"
                    multiple={true}
                    onChange={handleImportChange}
                />
                <Button href="https://gitlab.com/dashgl/mml2/" color="inherit">
                    Gitlab
                </Button>
            </Toolbar>
        </AppBar>
    )
}
